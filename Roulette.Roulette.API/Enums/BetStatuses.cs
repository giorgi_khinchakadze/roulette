namespace Roulette.Roulette.API.Enums
{
    public enum BetStatuses
    {
        Processing,
        Invalid,
        Rejected,
        Accepted,
        Completed
    }
}