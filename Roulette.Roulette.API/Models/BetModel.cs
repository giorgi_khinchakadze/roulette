using System;
using Roulette.Roulette.API.Enums;

namespace Roulette.Roulette.API.Models
{
    public class BetModel
    {
        public string Bet { get; set; }
    }

    public class BetViewModel
    {
        public Guid ID { get; set; }

        public decimal BetAmount { get; set; }
        public decimal WonAmount { get; set; }

        public DateTime CreateDate { get; set; }

        public BetStatuses Status { get; set; }
    }
}