using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.SignalR;
using Roulette.Contracts.Contracts;
using Roulette.Roulette.API.Enums;
using Roulette.Roulette.API.Hubs;
using Roulette.Roulette.API.Models;
using Roulette.Roulette.API.Persistence;
using Roulette.Roulette.API.Persistence.Entities;
using Roulette.Roulette.API.Services;

namespace Roulette.Roulette.API.Consumers
{
    public class BetRejectedConsumer : IConsumer<BetRejected>
    {
        private readonly ApplicationDbContext _context;
        private readonly IHubContext<RouletteHub> _hub;

        public BetRejectedConsumer(ApplicationDbContext context, IHubContext<RouletteHub> hub)
        {
            _context = context;
            _hub = hub;
        }

        /// <summary>
        ///  ამუშავებს ივენთს რო საკმარისი თანხა არ იყო და სიგნალრ ით ატყობინებს კლიენტს
        /// </summary>
        public async Task Consume(ConsumeContext<BetRejected> context)
        {
            var bet = await _context.Set<BetEntity>().FindAsync(context.Message.GameID);
            bet.Status = (byte) BetStatuses.Rejected;
            await _context.SaveChangesAsync();

            await _hub.Clients.User(bet.Subject)
                .SendAsync(
                    "update-bet",
                    new BetViewModel()
                    {
                        Status = (BetStatuses) bet.Status,
                        BetAmount = bet.BetAmount,
                        CreateDate = bet.CreateDate,
                        ID = bet.ID,
                        WonAmount = bet.WonAmount
                    }
                );
        }
    }
}