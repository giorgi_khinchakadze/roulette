using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using ge.singular.roulette;
using MassTransit;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Roulette.Contracts.Contracts;
using Roulette.Roulette.API.Enums;
using Roulette.Roulette.API.Hubs;
using Roulette.Roulette.API.Models;
using Roulette.Roulette.API.Persistence;
using Roulette.Roulette.API.Persistence.Entities;
using Roulette.Roulette.API.Services;

namespace Roulette.Roulette.API.Consumers
{
    public class BetAcceptedConsumer : IConsumer<BetAccepted>
    {
        private readonly ApplicationDbContext _context;
        private readonly IHubContext<RouletteHub> _hub;

        public BetAcceptedConsumer(ApplicationDbContext context, IHubContext<RouletteHub> hub)
        {
            _context = context;
            _hub = hub;
        }

        /// <summary>
        /// ამუშავებს ივენთს რო ფსონისთვის თანხა საკმარიას
        /// ამოწმებს ფსონი წარმატებულია თუ არა
        /// ცვლის ჯეკპოტს
        /// ორივეს შედეგს ატყობინებს კლიენტს სიგნალრ  
        /// </summary>
        public async Task Consume(ConsumeContext<BetAccepted> context)
        {
            var bet = await _context.Set<BetEntity>().FindAsync(context.Message.GameID);

            var winnum = RandomNumberGenerator.GetInt32(0, 37);

            var amount = CheckBets.EstimateWin(bet.Bet, winnum);

            bet.WonAmount = amount;
            bet.Status = (byte) BetStatuses.Completed;


            var jackpot = await _context.Set<JackpotEntity>().FirstOrDefaultAsync(a => a.Status == (byte) JackpotStatuses.Current);
            if (jackpot == null)
            {
                jackpot = new JackpotEntity()
                {
                    Status = (byte) JackpotStatuses.Current,
                    Amount = 0,
                };

                await _context.Set<JackpotEntity>().AddAsync(jackpot);
            }

            jackpot.Amount += bet.BetAmount / 100;


            await _context.SaveChangesAsync();


            await _hub.Clients.User(bet.Subject)
                .SendAsync(
                    "update-bet",
                    new BetViewModel()
                    {
                        Status = (BetStatuses) bet.Status,
                        BetAmount = bet.BetAmount,
                        CreateDate = bet.CreateDate,
                        ID = bet.ID,
                        WonAmount = bet.WonAmount
                    }
                );

            await _hub.Clients.All
                .SendAsync(
                    "update-jackpot",
                    new JackpotViewModel()
                    {
                        Amount = jackpot.Amount
                    }
                );
        }
    }
}