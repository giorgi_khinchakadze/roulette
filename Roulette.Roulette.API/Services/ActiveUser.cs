using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using Microsoft.AspNetCore.Http;

namespace Roulette.Roulette.API.Services
{
    public interface IActiveUser
    {
        public string Subject { get; }
    }

    public class ActiveUser : IActiveUser
    {
        private readonly IHttpContextAccessor _accessor;

        public ActiveUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string Subject =>
            _accessor.HttpContext.User.Identity.IsAuthenticated
                ? _accessor.HttpContext.User.Claims.First(a => a.Type == ClaimTypes.NameIdentifier).Value
                : throw new UnauthorizedAccessException();
    }
}