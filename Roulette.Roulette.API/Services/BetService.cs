using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using ge.singular.roulette;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Roulette.Contracts.Contracts;
using Roulette.Core.Enums;
using Roulette.Roulette.API.Enums;
using Roulette.Roulette.API.Models;
using Roulette.Roulette.API.Persistence;
using Roulette.Roulette.API.Persistence.Entities;

namespace Roulette.Roulette.API.Services
{
    public interface IBetService
    {
        Task<BetViewModel> ReceiveBetAsync(BetModel model);
        Task<JackpotViewModel> GetJackpotAsync();
    }

    public class BetService : IBetService
    {
        private readonly ApplicationDbContext _context;
        private readonly IPublishEndpoint _publish;
        private readonly IActiveUser _user;

        public BetService(ApplicationDbContext context, IPublishEndpoint publish, IActiveUser user)
        {
            _context = context;
            _publish = publish;
            _user = user;
        }


        public async Task<JackpotViewModel> GetJackpotAsync()
        {
            var jackpot = await _context.Set<JackpotEntity>().FirstOrDefaultAsync(a => a.Status == (byte) JackpotStatuses.Current);

            return new JackpotViewModel()
            {
                Amount = jackpot?.Amount ?? 0
            };
        }


        /// <summary>
        /// აქ რეგისტრირდება ფსონი
        /// ისვრის ივენთს რომ ფსონი დარეგისტრირდა
        /// account სერვისი იჭერს ივენთს და ნახულობს თუ ბალანსზე საკმარისი თანხაა აჭრის და ისვრის რო ფსონი მიღებულია
        /// თუ საკმარისი თანხა არაა ფსონს ეცვლება სტატუსი  რომ უარყოფილია 
        /// </summary>
        public async Task<BetViewModel> ReceiveBetAsync(BetModel model)
        {
            var check = CheckBets.IsValid(model.Bet);

            var entity = new BetEntity()
            {
                Bet = model.Bet,
                BetAmount = check.getBetAmount(),

                Status = (byte) (check.getIsValid() ? BetStatuses.Processing : BetStatuses.Invalid),
            };

            await _context.Set<BetEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();

            if (check.getIsValid())
            {
                await _publish.Publish(
                    new BetReceived()
                    {
                        Amount = entity.BetAmount,
                        Subject = _user.Subject,
                        GameType = (byte) GameTypes.Roulette,
                        GameID = entity.ID
                    }
                );
            }


            return new BetViewModel()
            {
                ID = entity.ID,
                Status = (BetStatuses) entity.Status,
                CreateDate = entity.CreateDate,
                BetAmount = entity.BetAmount
            };
        }
    }
}