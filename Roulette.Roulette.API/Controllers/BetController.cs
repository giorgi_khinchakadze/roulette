﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Roulette.Roulette.API.Models;
using Roulette.Roulette.API.Services;

namespace Roulette.Roulette.API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class BetController : ControllerBase
    {
        private readonly IBetService _service;

        public BetController(IBetService service)
        {
            _service = service;
        }


        [HttpPost]
        public async Task<BetViewModel> Bet(BetModel model)
        {
            var result = await _service.ReceiveBetAsync(model);

            return result;
        }


        [HttpGet("jackpot")]
        public async Task<JackpotViewModel> GetJackpot()
        {
            var jackpot = await _service.GetJackpotAsync();

            return jackpot;
        }
    }
}