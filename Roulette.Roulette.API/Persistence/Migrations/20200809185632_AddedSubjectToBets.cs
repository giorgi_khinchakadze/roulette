﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Roulette.Roulette.API.Persistence.Migrations
{
    public partial class AddedSubjectToBets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Subject",
                table: "JackpotEntity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Subject",
                table: "BetEntity",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Subject",
                table: "JackpotEntity");

            migrationBuilder.DropColumn(
                name: "Subject",
                table: "BetEntity");
        }
    }
}
