﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Roulette.Roulette.API.Persistence.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BetEntity",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Bet = table.Column<string>(nullable: true),
                    BetAmount = table.Column<decimal>(nullable: false),
                    WonAmount = table.Column<decimal>(nullable: false),
                    Status = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BetEntity", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "JackpotEntity",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    Status = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JackpotEntity", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BetEntity");

            migrationBuilder.DropTable(
                name: "JackpotEntity");
        }
    }
}
