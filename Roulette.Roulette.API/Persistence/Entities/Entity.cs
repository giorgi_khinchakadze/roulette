using System;

namespace Roulette.Roulette.API.Persistence.Entities
{
    public class Entity
    {
        public Guid ID { get; set; }

        public DateTime CreateDate { get; set; }

        public string Subject { get; set; }
    }
}