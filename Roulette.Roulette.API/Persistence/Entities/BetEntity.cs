namespace Roulette.Roulette.API.Persistence.Entities
{
    public class BetEntity : Entity
    {
        public string Bet { get; set; }

        public decimal BetAmount { get; set; }
        public decimal WonAmount { get; set; }

        public byte Status { get; set; }
    }
}