namespace Roulette.Roulette.API.Persistence.Entities
{
    public class JackpotEntity : Entity
    {
        public decimal Amount { get; set; }
        public byte Status { get; set; }
    }
}