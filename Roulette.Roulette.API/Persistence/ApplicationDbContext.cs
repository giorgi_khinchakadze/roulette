using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Roulette.Roulette.API.Persistence.Entities;
using Roulette.Roulette.API.Services;

namespace Roulette.Roulette.API.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        private readonly IActiveUser _user;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IActiveUser user)
            : base(options)
        {
            _user = user;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }

        public override Task<int> SaveChangesAsync(
            bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = new CancellationToken()
        )
        {
            foreach (var entry in ChangeTracker.Entries().Where(a => a.State == EntityState.Added))
            {
                if (entry.Entity is Entity entity)
                {
                    entity.CreateDate = DateTime.Now;
                    entity.Subject = _user.Subject;
                }
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
    }
}