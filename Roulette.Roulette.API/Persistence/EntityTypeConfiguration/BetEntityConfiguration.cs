using Roulette.Roulette.API.Persistence.Entities;

namespace Roulette.Roulette.API.Persistence.EntityTypeConfiguration
{
    public class JackpotEntityConfiguration : BaseEntityConfiguration<JackpotEntity>
    {
    }

    public class BetEntityConfiguration : BaseEntityConfiguration<BetEntity>
    {
    }
}