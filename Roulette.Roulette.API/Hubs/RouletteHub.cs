using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Roulette.Roulette.API.Hubs
{
    [Authorize]
    public class RouletteHub : Hub
    {
    }
}