using System;

namespace Roulette.Contracts.Contracts
{
    public class BetRejected
    {
        public byte GameType { get; set; }
        public Guid GameID { get; set; }
    }
}