using System;

namespace Roulette.Contracts.Contracts
{
    public class BetAccepted
    {
        public byte GameType { get; set; }
        public Guid GameID { get; set; }
    }
}