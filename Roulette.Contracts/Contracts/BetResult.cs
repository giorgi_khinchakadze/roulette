using System;

namespace Roulette.Contracts.Contracts
{
    public class BetResult
    {
        public byte GameType { get; set; }
        public Guid GameID { get; set; }

        public decimal Amount { get; set; }
    }
}