using System;

namespace Roulette.Contracts.Contracts
{
    public class BetReceived
    {
        public byte GameType { get; set; }
        public Guid GameID { get; set; }
        public decimal Amount { get; set; }
        public string Subject { get; set; }
    }
}