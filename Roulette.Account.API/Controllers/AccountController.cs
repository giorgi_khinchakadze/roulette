﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Roulette.Account.API.Models;
using Roulette.Account.API.Services;

namespace Roulette.Account.API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _service;
        private readonly IActiveUser _user;

        public AccountController(IAccountService service, IActiveUser user)
        {
            _service = service;
            _user = user;
        }


        [HttpGet]
        public async Task<AccountViewModel> GetAccount()
        {
            var account = await _service.GetAccountAsync(_user.Subject);

            return account;
        }

        [HttpGet("history/{type}")]
        public async Task<List<BetHistoryViewModel>> GetBettingHistory(byte type)
        {
            var list = await _service.GetBettingHistoryAsync(type, _user.Subject);

            return list;
        }
    }
}