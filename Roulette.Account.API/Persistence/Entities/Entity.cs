using System;

namespace Roulette.Account.API.Persistence.Entities
{
    public class Entity
    {
        public Guid ID { get; set; }

        public DateTime CreateDate { get; set; }
    }
}