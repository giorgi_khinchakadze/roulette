using System;

namespace Roulette.Account.API.Persistence.Entities
{
    public class BetHistory : Entity
    {
        public Guid AccountID { get; set; }

        public byte GameType { get; set; }
        public Guid GameID { get; set; }

        public decimal BetAmount { get; set; }
        public decimal WonAmount { get; set; }

        public byte Status { get; set; }

        public AccountEntity Account { get; set; }
    }
}