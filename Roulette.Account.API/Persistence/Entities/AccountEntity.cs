namespace Roulette.Account.API.Persistence.Entities
{
    public class AccountEntity : Entity
    {
        public string Subject { get; set; }
        public decimal Amount { get; set; }
    }
}