﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Roulette.Account.API.Persistence.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountEntity",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Subject = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountEntity", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "BetHistory",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    AccountID = table.Column<Guid>(nullable: false),
                    GameType = table.Column<byte>(nullable: false),
                    GameID = table.Column<Guid>(nullable: false),
                    BetAmount = table.Column<decimal>(nullable: false),
                    WonAmount = table.Column<decimal>(nullable: false),
                    Status = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BetHistory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BetHistory_AccountEntity_AccountID",
                        column: x => x.AccountID,
                        principalTable: "AccountEntity",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BetHistory_AccountID",
                table: "BetHistory",
                column: "AccountID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BetHistory");

            migrationBuilder.DropTable(
                name: "AccountEntity");
        }
    }
}
