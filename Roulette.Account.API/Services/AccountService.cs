using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Roulette.Account.API.Models;
using Roulette.Account.API.Persistence;
using Roulette.Account.API.Persistence.Entities;

namespace Roulette.Account.API.Services
{
    public interface IAccountService
    {
        Task<AccountViewModel> GetAccountAsync(string subject);
        Task<List<BetHistoryViewModel>> GetBettingHistoryAsync(byte type, string subject);
    }

    public class AccountService : IAccountService
    {
        private readonly ApplicationDbContext _context;

        public AccountService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<AccountViewModel> GetAccountAsync(string subject)
        {
            var account = await _context.Set<AccountEntity>().FirstAsync(a => a.Subject == subject);

            return new AccountViewModel()
            {
                Subject = subject,
                Amount = account.Amount
            };
        }

        public async Task<List<BetHistoryViewModel>> GetBettingHistoryAsync(byte type, string subject)
        {
            var betsQuery =
                from b in _context.Set<BetHistory>()
                join a in _context.Set<AccountEntity>() on b.AccountID equals a.ID
                where b.GameType == type && a.Subject == subject
                select b;

            var bets = await betsQuery.ToListAsync();

            var result = bets.Select(
                    a => new BetHistoryViewModel()
                    {
                        Subject = subject,
                        BetAmount = a.BetAmount,
                        WonAmount = a.WonAmount,
                        CreateDate = a.CreateDate,
                    }
                )
                .ToList();

            return result;
        }

    }
}