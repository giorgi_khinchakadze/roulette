using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebSockets;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Roulette.Account.API.Hubs;
using Roulette.Account.API.Persistence;
using Roulette.Account.API.Services;
using Roulette.Core.ExceptionHandling.Extensions;
using Roulette.Core.ExceptionHandling.Middleware;
using Roulette.Core.WebSocket.Middleware;

namespace Roulette.Account.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson().ConfigureExceptionHandling();


            services.AddDbContext<ApplicationDbContext>(
                builder =>
                    builder.UseMySql(Configuration.GetConnectionString("Account"))
            );

            services.AddMassTransit(
                configurator =>
                {
                    configurator.AddConsumers(Assembly.GetExecutingAssembly());

                    configurator.SetKebabCaseEndpointNameFormatter();
                    configurator.UsingRabbitMq(
                        (context, factoryConfigurator) =>
                        {
                            factoryConfigurator.Host(
                                Configuration["RabbitMQ:Host"],
                                hostConfigurator =>
                                {
                                    hostConfigurator.Username(Configuration["RabbitMQ:Username"]);
                                    hostConfigurator.Password(Configuration["RabbitMQ:Password"]);
                                }
                            );

                            factoryConfigurator.ConfigureEndpoints(context);
                        }
                    );
                }
            );

            services.AddMassTransitHostedService();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => Configuration.GetSection("Authentication").Bind(options));

            services.AddHttpContextAccessor();
            services.AddSingleton<IActiveUser, ActiveUser>();
            services.AddScoped<IAccountService, AccountService>();
            IdentityModelEventSource.ShowPII = true;


            services.AddCors(
                options =>
                {
                    options.AddPolicy(
                        "cors",
                        builder => builder.WithOrigins("https://localhost:1030").AllowAnyMethod().AllowAnyHeader().AllowCredentials()
                    );
                }
            );

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext context)
        {
            context.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware<ExceptionHandlingMiddleware>();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("cors");

            app.UseMiddleware<WebSocketAuthMiddleware>();
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapHub<AccountHub>("ws/account");
                }
            );
        }
    }
}