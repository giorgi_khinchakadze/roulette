namespace Roulette.Account.API.Enums
{
    public enum BetHistoryStatuses
    {
        Accepted,
        Completed
    }
}