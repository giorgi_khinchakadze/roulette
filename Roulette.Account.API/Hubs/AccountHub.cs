using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace Roulette.Account.API.Hubs
{
    [Authorize]
    public class AccountHub : Hub
    {
    }
}