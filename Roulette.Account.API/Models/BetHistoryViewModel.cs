using System;
using Roulette.Core.Enums;

namespace Roulette.Account.API.Models
{
    public class BetHistoryViewModel
    {
        public string Subject { get; set; }
        
        public GameTypes GameType { get; set; }

        public decimal BetAmount { get; set; }
        public decimal WonAmount { get; set; }

        public DateTime CreateDate { get; set; }
    }
}