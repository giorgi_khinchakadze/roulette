namespace Roulette.Account.API.Models
{
    public class AccountViewModel
    {
        public string Subject { get; set; }
        
        public decimal Amount { get; set; }
    }
}