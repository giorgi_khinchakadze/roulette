using System.Threading.Tasks;
using MassTransit;
using Roulette.Account.API.Persistence;
using Roulette.Account.API.Persistence.Entities;
using Roulette.Account.API.Services;
using Roulette.Contracts.Contracts;

namespace Roulette.Account.API.EventConsumers
{
    public class UserRegistrationConsumer : IConsumer<UserRegistered>
    {
        private readonly ApplicationDbContext _context;

        public UserRegistrationConsumer(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// მომმხმარებლის რეგისტრაციისას ხსნის ანგარიშ და აძლევს საჩუქარ თანხას 
        /// </summary>
        public async Task Consume(ConsumeContext<UserRegistered> context)
        {
            var entity = new AccountEntity()
            {
                Amount = 1000,
                Subject = context.Message.Subject,
            };

            await _context.Set<AccountEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }
    }
}