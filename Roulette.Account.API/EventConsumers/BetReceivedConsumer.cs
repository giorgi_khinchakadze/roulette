using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Roulette.Account.API.Enums;
using Roulette.Account.API.Hubs;
using Roulette.Account.API.Models;
using Roulette.Account.API.Persistence;
using Roulette.Account.API.Persistence.Entities;
using Roulette.Account.API.Services;
using Roulette.Contracts.Contracts;

namespace Roulette.Account.API.EventConsumers
{
    public class BetReceivedConsumer : IConsumer<BetReceived>
    {
        private readonly ApplicationDbContext _context;
        private readonly IPublishEndpoint _publish;
        private readonly IHubContext<AccountHub> _hub;

        public BetReceivedConsumer(ApplicationDbContext context, IPublishEndpoint publish, IHubContext<AccountHub> hub)
        {
            _context = context;
            _publish = publish;
            _hub = hub;
        }

        /// <summary>
        /// იღებს მოთხოვნას ფსონის რეგისტრაციაზე
        /// თუ საკმარისი თანხაა აჭრის თანხას და ატყობინებს სერვისს რომ ფსონი მიღებულია
        /// წინააღმდეგ შემთხვევაში ეუბნება რო ფსონი უარყოფილია 
        /// </summary>
        public async Task Consume(ConsumeContext<BetReceived> context)
        {
            var account = await _context.Set<AccountEntity>().FirstAsync(a => a.Subject == context.Message.Subject);

            if (account.Amount >= context.Message.Amount)
            {
                account.Amount -= context.Message.Amount;

                await _context.Set<BetHistory>()
                    .AddAsync(
                        new BetHistory()
                        {
                            Account = account,
                            Status = (byte) BetHistoryStatuses.Accepted,
                            BetAmount = context.Message.Amount,
                            GameType = context.Message.GameType,
                            GameID = context.Message.GameID,
                        }
                    );


                await _context.SaveChangesAsync();

                await _publish.Publish(
                    new BetAccepted()
                    {
                        GameType = context.Message.GameType,
                        GameID = context.Message.GameID,
                    }
                );

                await _hub.Clients.User(context.Message.Subject)
                    .SendAsync(
                        "update-account",
                        new AccountViewModel()
                        {
                            Subject = context.Message.Subject,
                            Amount = account.Amount
                        }
                    );
            }
            else
            {
                await _publish.Publish(
                    new BetRejected()
                    {
                        GameType = context.Message.GameType,
                        GameID = context.Message.GameID
                    }
                );
            }
        }
    }
}