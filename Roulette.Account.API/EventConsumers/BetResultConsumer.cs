using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Roulette.Account.API.Enums;
using Roulette.Account.API.Hubs;
using Roulette.Account.API.Models;
using Roulette.Account.API.Persistence;
using Roulette.Account.API.Persistence.Entities;
using Roulette.Account.API.Services;
using Roulette.Contracts.Contracts;

namespace Roulette.Account.API.EventConsumers
{
    public class BetResultConsumer : IConsumer<BetResult>
    {
        private readonly ApplicationDbContext _context;
        private readonly IHubContext<AccountHub> _hub;

        public BetResultConsumer(ApplicationDbContext context, IHubContext<AccountHub> hub)
        {
            _context = context;
            _hub = hub;
        }

        /// <summary>
        /// ფსონის დასრულებისას ანახლებს ანგარიშზე თანხას და სიგნალრ ით ატყობინებს კლიენტს 
        /// </summary>
        public async Task Consume(ConsumeContext<BetResult> context)
        {
            var bet = await _context.Set<BetHistory>()
                .Include(a => a.Account)
                .FirstAsync(a => a.GameID == context.Message.GameID && a.GameType == context.Message.GameType);

            bet.Status = (byte) BetHistoryStatuses.Completed;
            bet.WonAmount = context.Message.Amount;

            bet.Account.Amount += context.Message.Amount;

            await _context.SaveChangesAsync();


            await _hub.Clients.User(bet.Account.Subject)
                .SendAsync(
                    "update-account",
                    new AccountViewModel()
                    {
                        Subject = bet.Account.Subject,
                        Amount = bet.Account.Amount
                    }
                );
        }
    }
}