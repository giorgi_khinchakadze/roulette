using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace Roulette.Identity.Config
{
    public static class IdentityConfig
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("roulette", "Roulette API"),
                new ApiScope("account", "Account API")
            };


        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "front",

                    AllowedGrantTypes = GrantTypes.Code,

                    RedirectUris = {"https://localhost:1030", "https://localhost:1030/auth/login"},

                    PostLogoutRedirectUris = {"https://localhost:1030/auth/out"},
                    AllowedCorsOrigins = {"https://localhost:1030"},
                    AllowOfflineAccess = true,
                    Enabled = true,
                    RequireClientSecret = false,

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "roulette",
                        "account"
                    }
                }
            };
    }
}