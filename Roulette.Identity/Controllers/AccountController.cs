using System;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Events;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using MassTransit;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Roulette.Contracts.Contracts;
using Roulette.Identity.Attributes;
using Roulette.Identity.Models;
using Roulette.Identity.Persistence.Entities;

namespace Roulette.Identity.Controllers
{
    [AllowAnonymous]
    [SecurityHeaders]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IEventService _events;
        private readonly IPublishEndpoint _publish;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IIdentityServerInteractionService interaction,
            IEventService events,
            IPublishEndpoint publish
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _interaction = interaction;
            _events = events;
            _publish = publish;
        }

        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);

            return View(
                new LoginViewModel
                {
                    ReturnUrl = returnUrl,
                    Username = context?.LoginHint,
                }
            );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string button)
        {
            // check if we are in the context of an authorization request
            var context = await _interaction.GetAuthorizationContextAsync(model.ReturnUrl);

            // the user clicked the "cancel" button
            if (button != "login")
            {
                if (context == null) return Redirect("~/");

                // if the user cancels, send a result back into IdentityServer as if they 
                // denied the consent (even if this client does not require consent).
                // this will send back an access denied OIDC error response to the client.
                await _interaction.DenyAuthorizationAsync(context, AuthorizationError.AccessDenied);


                return Redirect(model.ReturnUrl);

                // since we don't have a valid context, then we just go back to the home page
            }

            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(
                    model.Username,
                    model.Password,
                    model.RememberLogin,
                    lockoutOnFailure: true
                );
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(model.Username);
                    await _events.RaiseAsync(
                        new UserLoginSuccessEvent(user.UserName, user.Id, user.UserName, clientId: context?.Client.ClientId)
                    );

                    if (context != null)
                    {
                        // we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
                        return Redirect(model.ReturnUrl);
                    }

                    if (Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }

                    if (string.IsNullOrEmpty(model.ReturnUrl))
                    {
                        return Redirect("~/");
                    }

                    throw new Exception("invalid return URL");
                }

                await _events.RaiseAsync(
                    new UserLoginFailureEvent(model.Username, "invalid credentials", clientId: context?.Client.ClientId)
                );
                ModelState.AddModelError(string.Empty, "invalid credentials");
            }

            return View(
                new LoginViewModel()
                {
                    Username = model.Username,
                    RememberLogin = model.RememberLogin,
                    ReturnUrl = model.ReturnUrl
                }
            );
        }


        [HttpGet]
        public IActionResult Register(string returnUrl)
        {
            return View(
                new RegisterViewModel
                {
                    ReturnUrl = returnUrl,
                }
            );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userIdentity = new ApplicationUser()
                {
                    UserName = model.UserName,
                    Email = model.UserName,
                    EmailConfirmed = true,
                };

                var createResult = await _userManager.CreateAsync(userIdentity, model.Password);

                if (createResult.Succeeded)
                {
                    await _signInManager.SignOutAsync();

                    await _publish.Publish(
                        new UserRegistered()
                        {
                            Subject = userIdentity.Id
                        }
                    );

                    return RedirectToAction(nameof(Login), "Account", new {model.ReturnUrl});
                }

                var skip = createResult.Errors.Any(e => e.Code == "DuplicateEmail");

                foreach (var error in createResult.Errors)
                {
                    if (error.Code == "DuplicateUserName" && skip)
                        continue;
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }


            return View(
                new RegisterViewModel()
                {
                    UserName = model.UserName,
                    ReturnUrl = model.ReturnUrl
                }
            );
        }

        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            // build a model so the logout page knows what to display


            var context = await _interaction.GetLogoutContextAsync(logoutId);

            var vm = new LogoutViewModel {LogoutId = logoutId};

            var silent = User?.Identity.IsAuthenticated == false || context?.ShowSignoutPrompt == false;

            // show the logout prompt. this prevents attacks where the user
            // is automatically signed out by another malicious web page.
            if (silent)
            {
                // if the request for logout was properly authenticated from IdentityServer, then
                // we don't need to show the prompt and can just log the user out directly.
                return await Logout(vm);
            }

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(LogoutViewModel model)
        {
            // get context information (client name, post logout redirect URI and iframe for federated signout)
            var logout = await _interaction.GetLogoutContextAsync(model.LogoutId);

            var vm = new LoggedOutViewModel
            {
                AutomaticRedirectAfterSignOut = true,
                PostLogoutRedirectUri = logout?.PostLogoutRedirectUri,
                ClientName = string.IsNullOrEmpty(logout?.ClientName) ? logout?.ClientId : logout?.ClientName,
                SignOutIframeUrl = logout?.SignOutIFrameUrl,
                LogoutId = model.LogoutId
            };


            if (User?.Identity.IsAuthenticated == true)
            {
                // delete local authentication cookie
                await _signInManager.SignOutAsync();

                // raise the logout event
                await _events.RaiseAsync(new UserLogoutSuccessEvent(User.GetSubjectId(), User.GetDisplayName()));
            }

            // check if we need to trigger sign-out at an upstream identity provider
            if (vm.TriggerExternalSignout)
            {
                // build a return URL so the upstream provider will redirect back
                // to us after the user has logged out. this allows us to then
                // complete our single sign-out processing.
                string url = Url.Action("Logout", new {logoutId = vm.LogoutId});

                // this triggers a redirect to the external provider for sign-out
                return SignOut(new AuthenticationProperties {RedirectUri = url}, vm.ExternalAuthenticationScheme);
            }

            return View("LoggedOut", vm);
        }
    }
}