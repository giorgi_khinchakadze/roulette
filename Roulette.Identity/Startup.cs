using System.Reflection;
using FluentValidation.AspNetCore;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Roulette.Identity.Persistence;
using Roulette.Identity.Persistence.Entities;

namespace Roulette.Identity
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation()
                .AddFluentValidation(configuration => configuration.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));

            services.AddDbContext<ApplicationDbContext>(
                builder =>
                    builder.UseMySql(Configuration.GetConnectionString("Identity"))
            );

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddIdentityServer(
                    options =>
                    {
                        options.IssuerUri = Configuration["IdentityServer:IssuerUri"];
                        options.Events.RaiseErrorEvents = true;
                        options.Events.RaiseFailureEvents = true;
                        options.Events.RaiseInformationEvents = true;
                        options.Events.RaiseSuccessEvents = true;

                        options.EmitStaticAudienceClaim = true;
                    }
                )
                .AddInMemoryIdentityResources(Config.IdentityConfig.IdentityResources)
                .AddInMemoryApiScopes(Config.IdentityConfig.ApiScopes)
                .AddInMemoryClients(Config.IdentityConfig.Clients)
                .AddAspNetIdentity<ApplicationUser>()
                .AddDeveloperSigningCredential();

            services.AddAuthentication();

            services.AddMassTransit(
                configurator =>
                {
                    configurator.AddConsumers(Assembly.GetExecutingAssembly());

                    configurator.SetKebabCaseEndpointNameFormatter();
                    configurator.UsingRabbitMq(
                        (context, factoryConfigurator) =>
                        {
                            factoryConfigurator.Host(
                                Configuration["RabbitMQ:Host"],
                                hostConfigurator =>
                                {
                                    hostConfigurator.Username(Configuration["RabbitMQ:Username"]);
                                    hostConfigurator.Password(Configuration["RabbitMQ:Password"]);
                                }
                            );
                            factoryConfigurator.ConfigureEndpoints(context);
                        }
                    );
                }
            );

            services.AddMassTransitHostedService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext context)
        {
            context.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseIdentityServer();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapDefaultControllerRoute(); });
        }
    }
}