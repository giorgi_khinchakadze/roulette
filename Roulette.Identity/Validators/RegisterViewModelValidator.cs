using FluentValidation;
using Roulette.Identity.Models;

namespace Roulette.Identity.Validators
{
    public class RegisterViewModelValidator : AbstractValidator<RegisterViewModel>
    {
        public RegisterViewModelValidator()
        {
            RuleFor(r => r.UserName)
                .NotEmpty()
                .WithMessage("მიუთითეთ მეილი")
                .EmailAddress()
                .WithMessage("არავალიდური მეილი");

            RuleFor(r => r.Password)
                .NotEmpty()
                .WithMessage("მიუთითეთ პაროლი");

            RuleFor(r => r.ConfirmPassword)
                .Equal(e => e.Password)
                .WithMessage("პაროლი არ ემთხვევა");
        }
    }
}