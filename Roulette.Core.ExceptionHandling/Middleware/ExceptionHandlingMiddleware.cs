﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Roulette.Core.ExceptionHandling.Extensions;

namespace Roulette.Core.ExceptionHandling.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception)
            {
                if (!context.Request.AcceptsJson()) throw;

                context.Response.StatusCode = 500;
                context.Response.ContentType = "Application/json";
                context.Response.Body.SetLength(0);
            }
        }
    }
}