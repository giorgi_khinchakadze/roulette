﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Roulette.Core.ExceptionHandling.Filters;
using Roulette.Core.Models;

namespace Roulette.Core.ExceptionHandling.Extensions
{
    public static class ExceptionHandlingExtensions
    {
        private static readonly string[] JsonMimeTypes = {"*/*", "application/*", "application/json"};

        public static bool AcceptsJson(this HttpRequest request) =>
            request.Headers.ContainsKey("Accept")
                ? request.Headers["Accept"]
                    .Select(s => s.ToLowerInvariant())
                    .Any(accept => JsonMimeTypes.Any(accept.Contains))
                : request.ContentType.Split(";")
                    .Select(s => s.Trim())
                    .Where(s => !string.IsNullOrWhiteSpace(s))
                    .Any(s => s == JsonMimeTypes[2]);


        public static BadRequestObjectResult ToBadRequestObject(this ValidationException exception) =>
            new BadRequestObjectResult(
                new BadRequest()
                {
                    Errors = new List<ValidationError>()
                    {
                        new ValidationError()
                        {
                            Message = exception.ValidationResult.ErrorMessage,
                            Property = string.Join("; ", exception.ValidationResult.MemberNames),
                        }
                    },
                }
            );

        public static IMvcBuilder ConfigureExceptionHandling(this IMvcBuilder builder)
        {
            builder.Services.Configure<MvcOptions>(options => options.Filters.Add<AsyncExceptionHandlingFilter>());

            return builder;
        }
    }
}