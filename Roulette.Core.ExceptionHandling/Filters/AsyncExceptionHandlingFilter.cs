﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Roulette.Core.ExceptionHandling.Extensions;

namespace Roulette.Core.ExceptionHandling.Filters
{
    public class AsyncExceptionHandlingFilter : IAsyncExceptionFilter
    {
        private static readonly JsonSerializerSettings SerializerSettings =
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };


        public Task OnExceptionAsync(ExceptionContext context)
        {
            if (!context.HttpContext.Request.AcceptsJson()) return Task.CompletedTask;

            var env = context.HttpContext.RequestServices.GetRequiredService<Microsoft.AspNetCore.Hosting.IWebHostEnvironment>();

            if (context.Exception is UnauthorizedAccessException)
            {
                context.Result = new UnauthorizedResult();
                context.ExceptionHandled = true;
            }
            else if (context.Exception is ValidationException validation)
            {
                context.Result = validation.ToBadRequestObject();
                context.ExceptionHandled = true;
            }
            else if (env.IsDevelopment())
            {
                context.Result = new JsonResult(context.Exception?.InnerException ?? context.Exception, SerializerSettings)
                {
                    StatusCode = StatusCodes.Status500InternalServerError
                };
            }
            else
            {
                context.Result = new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Task.CompletedTask;
        }
    }
}