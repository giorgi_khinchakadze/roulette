﻿﻿using System.Collections.Generic;

 namespace Roulette.Core.Models
{
    public class BadRequest
    {
        public List<ValidationError> Errors { get; set; } = new List<ValidationError>();
    }
}