﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Roulette.Web.Options;

namespace Roulette.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SettingsController : ControllerBase
    {
        private readonly Settings _settings;

        public SettingsController(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        [HttpGet]
        public Settings GetSettings()
        {
            return _settings;
        }
    }
}