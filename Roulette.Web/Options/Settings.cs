namespace Roulette.Web.Options
{
    public class Settings
    {
        public string Identity { get; set; }
        public string Roulette { get; set; }
        public string Account { get; set; }
    }
}