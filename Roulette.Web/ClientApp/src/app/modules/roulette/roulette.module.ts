import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouletteRoutingModule } from './roulette-routing.module';
import { RouletteComponent } from './roulette.component';
import {DxButtonModule, DxDataGridModule, DxTextAreaModule} from 'devextreme-angular';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [RouletteComponent],
  imports: [
    CommonModule,
    RouletteRoutingModule,
    DxTextAreaModule,
    ReactiveFormsModule,
    DxButtonModule,
    DxDataGridModule,
  ],
})
export class RouletteModule { }
