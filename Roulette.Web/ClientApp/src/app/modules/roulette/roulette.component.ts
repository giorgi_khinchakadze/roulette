import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {RouletteService} from './roulette.service';
import {BehaviorSubject, Subject} from 'rxjs';
import {BetStatuses, BetViewModel, JackpotViewModel} from './roulette.model';
import {takeUntil} from 'rxjs/operators';
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';

@Component({
  selector: 'app-roulette',
  templateUrl: './roulette.component.html',
  styleUrls: ['./roulette.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouletteComponent implements OnInit,
                                          OnDestroy {


  private unsubscribe$ = new Subject();

  bet: string;

  jackpot$ = new BehaviorSubject<JackpotViewModel>(null);

  currentBets: BetViewModel[] = [];

  hub: HubConnection;

  betStatuses = [
    {
      id: BetStatuses.Accepted,
      text: 'Accepted',
    },
    {
      id: BetStatuses.Completed,
      text: 'Completed',
    },
    {
      id: BetStatuses.Invalid,
      text: 'Invalid',
    },
    {
      id: BetStatuses.Processing,
      text: 'Processing',
    },
    {
      id: BetStatuses.Rejected,
      text: 'Rejected',
    },
  ];


  constructor(private service: RouletteService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.service.getJackpot().pipe(takeUntil(this.unsubscribe$)).subscribe(a => this.jackpot$.next(a));
    this.service.jackpotChanged$.pipe(takeUntil(this.unsubscribe$)).subscribe(a => this.jackpot$.next(a));

    this.service.betChanged$.pipe(takeUntil(this.unsubscribe$)).subscribe(a => this.updateBet(a));
  }


  async makeBet() {
    this.service.makeBet(this.bet).pipe(takeUntil(this.unsubscribe$)).subscribe(a => {
      this.currentBets.push(a);
      this.cdr.markForCheck();
    });
  }

  updateBet(bet: BetViewModel) {
    let idx = this.currentBets.findIndex(a => a.id == bet.id);

    if (idx > -1) {

      this.currentBets[idx].status = bet.status;
      this.currentBets[idx].wonAmount = bet.wonAmount;

      this.cdr.markForCheck();
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
