import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from '../../shared/settings';
import {BetViewModel, JackpotViewModel} from './roulette.model';
import {Observable, Subject} from 'rxjs';
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import {AuthService} from '../auth/core';


@Injectable({
  providedIn: 'root',
})
export class RouletteService implements OnDestroy {

  private hub: HubConnection;

  betChanged$ = new Subject<BetViewModel>();
  jackpotChanged$ = new Subject<JackpotViewModel>();

  constructor(private client: HttpClient, private settings: SettingsService, private auth: AuthService) {
    this.hub = new HubConnectionBuilder().withUrl(
      `${this.settings.settings$.value.roulette}/ws/roulette`,
      {
        accessTokenFactory: () => this.auth.getAccessToken(),
      },
    ).build();
    this.hub.start();

    this.hub.on('update-bet', args => this.betChanged$.next(args));
    this.hub.on('update-jackpot', args => this.jackpotChanged$.next(args));
  }

  public makeBet(bet: string): Observable<BetViewModel> {
    return this.client.post<BetViewModel>(`${this.settings.settings$.value.roulette}/api/bet`, {bet: bet});
  }

  public getJackpot(): Observable<JackpotViewModel> {
    return this.client.get<JackpotViewModel>(`${this.settings.settings$.value.roulette}/api/bet/jackpot`);
  }

  ngOnDestroy(): void {
    this.hub.stop();
    this.betChanged$.complete();
    this.jackpotChanged$.complete();
  }
}
