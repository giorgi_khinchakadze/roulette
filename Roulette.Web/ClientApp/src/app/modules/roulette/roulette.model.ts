export enum BetStatuses {
  Processing,
  Invalid,
  Rejected,
  Accepted,
  Completed
}

export interface BetViewModel {
  id: string;
  betAmount: number;
  wonAmount: number;

  createDate: Date;

  status: BetStatuses
}

export interface JackpotViewModel {
  amount: number;
}
