﻿import {OAuthResourceServerErrorHandler} from "angular-oauth2-oidc";
import {Injectable} from "@angular/core";
import {HttpResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Store} from "@ngrx/store";
import {ForbiddenResourceRequested, Login} from "./auth.actions";
import {AppState} from "../../../shared/core/reducers";

@Injectable({
  providedIn:"root"
})
export class HttpAuthErrorHandler implements OAuthResourceServerErrorHandler {

  constructor(private store: Store<AppState>) {
  }

  handleError(err: HttpResponse<any>): Observable<any> {
    if (err.status == 401) {
      this.store.dispatch(new Login({returnUrl: window.location.pathname}));
    }
    if (err.status == 403) {
      this.store.dispatch(new ForbiddenResourceRequested())
    }

    return throwError(err);
  }
}

@Injectable({
  providedIn:"root"
})
export class StoreAuthErrorHandler {
  constructor(private store: Store<AppState>) {
  }

  handleStoreError(err: { httpStatus?: number, errorDetails?: any, requestOptions?: any }) {
    if (err.httpStatus == 401) {
      this.store.dispatch(new Login({returnUrl: window.location.pathname}));
    }
    if (err.httpStatus == 403) {
      this.store.dispatch(new ForbiddenResourceRequested())
    }
    return throwError(err);
  }

}
