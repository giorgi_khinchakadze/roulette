﻿import {Action} from "@ngrx/store";
import {User} from "./auth.models";

export enum AuthActionsTypes {
    UserRequested = '[Request User] Auth Action',
    UserLoaded = '[User Loaded] Auth Action',
    Logout = '[Logout] Auth Action',
    LoggedIn = '[Logged In] Auth Action',
    Login = '[Login] Auth Action',
    ForbiddenResourceRequested = '[Forbidden Requested] Auth Action',

}

export class UserRequested implements Action {
    readonly type = AuthActionsTypes.UserRequested
}

export class UserLoaded implements Action {
    readonly type = AuthActionsTypes.UserLoaded;

    constructor(public payload: { user: User }) {
    }
}

export class LoggedIn implements Action {
    readonly type = AuthActionsTypes.LoggedIn;

    constructor(public payload: { state: any }) {
    }
}

export class Logout implements Action {
    readonly type = AuthActionsTypes.Logout
}

export class Login implements Action {
    readonly type = AuthActionsTypes.Login;

    constructor(public payload: { returnUrl: string }) {
    }
}

export class ForbiddenResourceRequested implements Action {
    readonly type = AuthActionsTypes.ForbiddenResourceRequested

}

export type AuthActions = UserRequested | UserLoaded | Logout | LoggedIn | Login | ForbiddenResourceRequested;
