﻿export class User {
  id: string;
  email: string;
  pic: string;

  clear(): void {
    this.id = undefined;
    this.email = '';
    this.pic = './assets/media/users/default.jpg';
  }
}
