﻿import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {AuthService} from './auth.service';
import {AuthActionsTypes, LoggedIn, Login, Logout, UserLoaded, UserRequested} from './auth.actions';
import {filter, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import {isUserLoaded} from './auth.selectors';
import {User} from './auth.models';
import {AppState} from '../../../shared/core/reducers';


@Injectable()
export class AuthEffects {
  loggedIn$ =
    createEffect(
      () =>
        this.actions$.pipe(
          ofType<LoggedIn>(AuthActionsTypes.LoggedIn),
          tap(x => {
            this.router.navigateByUrl(decodeURIComponent(x.payload.state));
            this.store.dispatch(new UserRequested());
          }),
        ),
      {dispatch: false},
    );

  login$ =
    createEffect(
      () =>
        this.actions$.pipe(
          ofType<Login>(AuthActionsTypes.Login),
          tap(x => {
            this.router.navigate(['/auth/login'], {queryParams: {returnUrl: x.payload.returnUrl}});
          }),
        ),
      {dispatch: false},
    );

  logout$ =
    createEffect(
      () =>
        this.actions$.pipe(
          ofType<Logout>(AuthActionsTypes.Logout),
          tap(x => this.auth.logOut()),
        ),
      {dispatch: false},
    );

  userRequested$ =
    createEffect(() =>
      this.actions$.pipe(
        ofType<UserRequested>(AuthActionsTypes.UserRequested),
        withLatestFrom(this.store.pipe(select(isUserLoaded))),
        filter(([action, _isUserLoaded]) => !_isUserLoaded),
        mergeMap(() =>
          this.auth.loadUserInfo().pipe(
            tap((user: User) =>
              this.store.dispatch(
                user
                ? new UserLoaded({user: user})
                : new Logout())),
          )),
      ), {dispatch: false});

  constructor(
    private actions$: Actions,
    private router: Router,
    private auth: AuthService,
    private store: Store<AppState>,
  ) {
  }
}
