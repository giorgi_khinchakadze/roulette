﻿import {createSelector} from "@ngrx/store";
import {AuthState} from "./auth.reducers";
import {User} from "./auth.models";
import {AppState} from "../../../shared/core/reducers";


export const authState = state => state.auth || <AuthState>{};

export const isLoggedIn = createSelector<AppState, AuthState, boolean>(authState, auth => auth.loggedIn);
export const isLoggedOut = createSelector<AppState, AuthState, boolean>(authState, auth => !auth.loggedIn);
export const isUserLoaded = createSelector<AppState, AuthState, boolean>(authState, auth => auth.isUserLoaded);
export const currentUser = createSelector<AppState, AuthState, User>(authState, auth => auth.user);
