﻿import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";
import {Store} from "@ngrx/store";
import {Login} from "./auth.actions";
import {AppState} from "../../../shared/core/reducers";


@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AppState>, private auth: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.hasValidIdToken()) {
      return true;
    } else {
      this.store.dispatch(new Login({returnUrl: state.url}));
      return false;
    }
  }
}
