﻿import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable, Optional} from "@angular/core";
import {OAuthModuleConfig, OAuthStorage} from "angular-oauth2-oidc";
import {HttpAuthErrorHandler, StoreAuthErrorHandler} from "./auth.handlers";
import {catchError} from "rxjs/operators";


@Injectable({
  providedIn:"root"
})
export class HttpAuthInterceptor implements HttpInterceptor {

  constructor(private errorHandler: HttpAuthErrorHandler,
              private authStorage: OAuthStorage,
              @Optional() private moduleConfig: OAuthModuleConfig
  ) {
  }

  private checkUrl(url: string): boolean {
    let found = this.moduleConfig.resourceServer.allowedUrls.find(u => url.startsWith(u));
    return !!found;
  }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let url = req.url.toLowerCase();

    if (!this.moduleConfig) return next.handle(req);
    if (!this.moduleConfig.resourceServer) return next.handle(req);
    if (!this.moduleConfig.resourceServer.allowedUrls) return next.handle(req);
    if (!this.checkUrl(url)) return next.handle(req);

    let sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;

    if (sendAccessToken) {

      let token = this.authStorage.getItem('access_token');
      let header = 'Bearer ' + token;

      let headers = req.headers
        .set('Authorization', header);

      req = req.clone({headers});
    }

    return next.handle(req).pipe(catchError(err => this.errorHandler.handleError(err)));

  }

}

@Injectable({
  providedIn:"root"
})
export class StoreAuthInterceptor {
  constructor(private errorHandler: StoreAuthErrorHandler,
              private authStorage: OAuthStorage,
              @Optional() private moduleConfig: OAuthModuleConfig
  ) {
  }


  private checkUrl(url: string): boolean {
    let found = this.moduleConfig.resourceServer.allowedUrls.find(u => url.startsWith(u));
    return !!found;
  }

  interceptStore(options: { url?: string, async?: boolean, method?: string, timeout?: number, params?: any, payload?: any, headers?: any }) {
    let url = options.url.toLocaleLowerCase();

    if (!this.moduleConfig) return;
    if (!this.moduleConfig.resourceServer) return;
    if (!this.moduleConfig.resourceServer.allowedUrls) return;
    if (!this.checkUrl(url)) return;

    let sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;

    if (sendAccessToken) {
      let token = this.authStorage.getItem('access_token');
      options.headers['Authorization'] = 'Bearer ' + token;
    }

  }
}
