﻿import {OAuthService} from 'angular-oauth2-oidc';
import {from, Observable} from 'rxjs';
import {User} from './auth.models';
import {map} from 'rxjs/operators';
import {authConfig} from './auth.config';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends OAuthService {

  loadUserInfo(): Observable<User> {
    return from(this.loadUserProfile()).pipe(map(result =>
      <User>{
        id: result['sub'],
        email: result['name'],
        pic: result['pic'] || './assets/media/users/default.jpg',
      }));
  }

  configureAndTryLogin(url: string): Promise<boolean> {
    this.configure({...authConfig, issuer: url});

    return this.loadDiscoveryDocumentAndTryLogin();
  }
}
