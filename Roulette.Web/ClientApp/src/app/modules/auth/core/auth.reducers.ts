﻿import {User} from "./auth.models";
import {AuthActions, AuthActionsTypes} from "./auth.actions";


export interface AuthState {
  loggedIn: boolean,
  user: User,
  isUserLoaded: boolean,
  isLoading: boolean
}

export const initialAuthState: AuthState = {
  isUserLoaded: false,
  loggedIn: false,
  user: undefined,
  isLoading: false
};


export function authReducer(state = initialAuthState, action: AuthActions): AuthState {
  let newState = {...state};
  switch (action.type) {
    case AuthActionsTypes.Logout:
      newState = {
        ...initialAuthState
      };
      break;
    case AuthActionsTypes.Login:
      newState = {
        ...initialAuthState
      };
      break;
    case AuthActionsTypes.LoggedIn:
      newState = {
        ...state,
        loggedIn: true
      };
      break;
    case AuthActionsTypes.UserLoaded:
      newState = {
        ...state,
        user: action.payload.user,
        isLoading: false,
        isUserLoaded: true,
      };
      break;
    case AuthActionsTypes.UserRequested:
      newState = {
        ...state,
        isLoading: true
      };
      break;

  }
  return newState;
}
