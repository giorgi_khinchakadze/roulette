﻿import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  redirectUri: window.location.origin,
  postLogoutRedirectUri: window.location.origin + '/auth/out',
  clientId: 'front',
  responseType: 'code',
  scope: 'openid profile offline_access roulette account',
  showDebugInformation: true,
};
