import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModuleWithProviders} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {OAuthModule} from "angular-oauth2-oidc";
import {StoreModule} from "@ngrx/store";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {
  AuthEffects,
  authReducer,
  HttpAuthInterceptor,
} from "./core";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EffectsModule.forFeature([AuthEffects]),
    StoreModule.forFeature('auth', authReducer),
    OAuthModule
  ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HttpAuthInterceptor,
          multi: true,
        },
      ],
    }
  }
}
