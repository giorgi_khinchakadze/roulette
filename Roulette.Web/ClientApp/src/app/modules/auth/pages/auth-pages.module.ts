import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthPagesRoutingModule} from './auth-pages-routing.module';
import {LoginComponent} from './login/login.component';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    AuthPagesRoutingModule,
  ],
})
export class AuthPagesModule {
}
