import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ChangeDetectionStrategy} from "@angular/core";
import {AuthService} from "../../core";

@Component({
  selector: 'kt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {

  constructor(private service: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.service.initLoginFlow(this.route.snapshot.queryParams.returnUrl)
  }

}
