export interface AccountViewModel {
  subject: string;
  amount: number;
}

export interface GameTypes {
  Roulette
}

export interface BetHistoryViewModel {

  betAmount: number;
  wonAmount: number;

  createDate: Date;
}

