import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from '../../shared/settings';
import {Observable, Subject} from 'rxjs';
import {AccountViewModel, BetHistoryViewModel, GameTypes} from './account.model';
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import {Arg} from '@aspnet/signalr/dist/esm/Utils';
import {AuthService} from '../auth/core';

@Injectable({
  providedIn: 'root',
})
export class AccountService {

  private hub: HubConnection;

  accountChanged$ = new Subject<AccountViewModel>();

  constructor(private client: HttpClient, private settings: SettingsService, private auth: AuthService) {
    this.hub = new HubConnectionBuilder().withUrl(
      `${this.settings.settings$.value.account}/ws/account`,
      {
        accessTokenFactory: () => this.auth.getAccessToken(),
      },
    ).build();
    this.hub.start();

    this.hub.on('update-account', args => this.accountChanged$.next(args));
  }


  public getAccount(): Observable<AccountViewModel> {
    return this.client.get<AccountViewModel>(`${this.settings.settings$.value.account}/api/account`);
  }

  public getBettingHistory(type: GameTypes): Observable<BetHistoryViewModel[]> {
    return this.client.get<BetHistoryViewModel[]>(`${this.settings.settings$.value.account}/api/account/history/${type}`);
  }

  ngOnDestroy(): void {
    this.hub.stop();
    this.accountChanged$.complete();
  }
}
