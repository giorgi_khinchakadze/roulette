import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './modules/auth/core';
import {BaseComponent} from './shared/layout/base/base.component';


const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('src/app/modules/auth/pages/auth-pages.module').then(value => value.AuthPagesModule),
  },
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'roulette',
        loadChildren: () => import('src/app/modules/roulette/roulette.module').then(a => a.RouletteModule),
      },
      {
        path: '',
        redirectTo: 'roulette',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'disabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
