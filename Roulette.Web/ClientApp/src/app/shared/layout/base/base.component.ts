import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AccountViewModel} from '../../../modules/account/account.model';
import {AccountService} from '../../../modules/account/account.service';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BaseComponent implements OnInit,
                                      OnDestroy {

  private unsubscribe$ = new Subject();

  account$ = new BehaviorSubject<AccountViewModel>(null);

  constructor(private service: AccountService) { }

  ngOnInit(): void {
    this.service.getAccount().pipe(takeUntil(this.unsubscribe$)).subscribe(a => this.account$.next(a));
    this.service.accountChanged$.pipe(takeUntil(this.unsubscribe$)).subscribe(a => this.account$.next(a));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
