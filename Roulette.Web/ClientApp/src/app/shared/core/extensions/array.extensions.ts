interface MapExpression<T, TD> {
  (obj: T): TD;
}

declare global {
  interface Array<T> {
    flatMap<TD>(map: MapExpression<T, Array<TD>>): Array<TD>,
  }
}


Array.prototype.flatMap = function (map) {
  return this.map(map).reduce((pv, cv) => [...pv, ...cv], [])
}

export {};
