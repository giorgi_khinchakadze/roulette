import * as moment from 'moment';

declare global {
  interface Date {
    isOfAge(): boolean;
  }
}


Date.prototype.isOfAge = function () {
  return moment().diff(this, 'year') > 17;
};

export {};
