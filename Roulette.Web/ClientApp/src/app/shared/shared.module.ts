import {ModuleWithProviders, NgModule} from '@angular/core';
import {SettingsModule} from './settings/settings.module';
import {BaseComponent} from './layout/base/base.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';


@NgModule({
  declarations: [BaseComponent],
  imports: [
    RouterModule,
    CommonModule,
  ],
  exports: [
    SettingsModule,
  ],
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [],
    };
  }
}
