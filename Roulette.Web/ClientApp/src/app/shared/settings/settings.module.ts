import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {SettingsEffects} from './settings.effects';
import {StoreModule} from '@ngrx/store';
import {settingsReducer} from './settings.reducers';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EffectsModule.forFeature([SettingsEffects]),
    StoreModule.forFeature('settings', settingsReducer),
  ],
})
export class SettingsModule {}
