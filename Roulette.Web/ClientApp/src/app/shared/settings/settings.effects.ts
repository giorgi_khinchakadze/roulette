import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {SettingsService} from './settings.service';
import {SettingsActionTypes, SettingsLoaded, SettingsRequested} from './settings.actions';
import {map, switchMap} from 'rxjs/operators';

@Injectable()
export class SettingsEffects {
  settingsRequested$ =
    createEffect(
      () =>
        this.actions$.pipe(
          ofType<SettingsRequested>(SettingsActionTypes.SettingsRequested),
          switchMap(value => this.service.load()),
          map(value => new SettingsLoaded({settings: value})),
        ),
    );

  constructor(
    private actions$: Actions,
    private service: SettingsService,
  ) {}
}
