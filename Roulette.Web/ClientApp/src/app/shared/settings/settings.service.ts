import {BehaviorSubject, Observable} from 'rxjs';
import {SettingsModel} from './settings.models';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Injectable, OnDestroy} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SettingsService implements OnDestroy {

  public settings$ = new BehaviorSubject<SettingsModel>({});
  public initialized$ = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
  }

  public load(): Observable<SettingsModel> {
    return this.http.get<SettingsModel>('api/settings').pipe(
      tap(value => this.settings$.next(value)),
      tap(a => this.initialized$.next(true)),
    );
  }

  ngOnDestroy() {
    this.settings$.complete();
    this.initialized$.complete();
  }
}
