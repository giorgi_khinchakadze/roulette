import {SettingsState} from './settings.reducers';
import {createSelector} from '@ngrx/store';
import {AppState} from '../core/reducers';


export const settingsState = state => state.settings || <SettingsState>{};

export class SettingsSelects {
  static settings = createSelector<AppState, SettingsState, boolean>(settingsState, s1 => !!s1.settings);
}
