import {Action} from '@ngrx/store';
import {SettingsModel} from './settings.models';


export enum SettingsActionTypes {
  SettingsRequested = '[Settings Requested] Settings Actions',
  SettingsLoaded = '[Settings Loaded] Settings Actions',
}

export class SettingsRequested implements Action {
  readonly type = SettingsActionTypes.SettingsRequested;
}

export class SettingsLoaded implements Action {
  readonly type = SettingsActionTypes.SettingsLoaded;

  constructor(public payload: { settings: SettingsModel }) {}
}

export type SettingsActions = SettingsRequested | SettingsLoaded;
