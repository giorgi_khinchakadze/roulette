import {SettingsModel} from './settings.models';
import {SettingsActions, SettingsActionTypes} from './settings.actions';


export interface SettingsState {
  settings?: SettingsModel
}

export const initialSettingsState: SettingsState = {};

export function settingsReducer(state = initialSettingsState, action: SettingsActions): SettingsState {
  let newState = {...state};

  switch (action.type) {
    case SettingsActionTypes.SettingsRequested:
      newState.settings = null;
      break;
    case SettingsActionTypes.SettingsLoaded:
      newState.settings = action.payload.settings;
      break;
  }

  return newState;

}

