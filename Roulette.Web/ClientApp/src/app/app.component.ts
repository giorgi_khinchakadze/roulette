import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from './shared/core/reducers';
import {SettingsRequested, SettingsSelects, SettingsService} from './shared/settings';
import {AuthService, LoggedIn} from './modules/auth/core';
import {filter, first, map, mergeMap, takeUntil, tap} from 'rxjs/operators';
import {forkJoin, Subject} from 'rxjs';
import {OAuthModuleConfig} from 'angular-oauth2-oidc';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit,
                                     OnDestroy {


  private unsubscribe$ = new Subject();

  loader: boolean = true;


  constructor(
    private store: Store<AppState>,
    private auth: AuthService,
    private oAuthModuleConfig: OAuthModuleConfig,
    private settings: SettingsService,
    private router: Router,
  ) {
    this.store.dispatch(new SettingsRequested());
  }

  ngOnInit(): void {
    this.store.select(SettingsSelects.settings)
      .pipe(
        filter(value => !!value),
        first(),
        takeUntil(this.unsubscribe$),
      )
      .subscribe(value => {
        this.oAuthModuleConfig.resourceServer.allowedUrls.push(this.settings.settings$.value.account);
        this.oAuthModuleConfig.resourceServer.allowedUrls.push(this.settings.settings$.value.identity);
        this.oAuthModuleConfig.resourceServer.allowedUrls.push(this.settings.settings$.value.roulette);

        this.auth.configureAndTryLogin(this.settings.settings$.value.identity)
          .then(value => {
            this.router.initialNavigation();

            if (this.auth.hasValidAccessToken() && this.auth.hasValidIdToken()) {
              this.store.dispatch(new LoggedIn({state: this.auth.state || window.location.pathname}));
            }
          });
      });

    forkJoin(
      [
        this.store.select(SettingsSelects.settings).pipe(filter(a => !!a), first()),
        this.router.events.pipe(filter(a => a instanceof NavigationEnd), first()),
      ],
    ).pipe(
      takeUntil(this.unsubscribe$),
      tap(a => this.loader = false),
    ).subscribe();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
